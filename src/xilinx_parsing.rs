use anyhow::{anyhow, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;

use crate::parseutil::LinesExt;

pub trait XilinxExt {
    // General functions
    fn strip_toc(&self) -> Result<Vec<String>>;
    fn take_section(&self) -> Result<Vec<String>>;
    fn skip_to_section(&self, section: &str) -> Result<Vec<String>>;
    fn table_row(&self, name: &str) -> Result<TableRow>;

    // Concrete functions for getting values
    fn slice_luts(&self) -> Result<usize>;
    fn slice_registers(&self) -> Result<usize>;
    fn slices(&self) -> Result<usize>;
    fn bram(&self) -> Result<usize>;
}

impl XilinxExt for [String] {
    fn strip_toc(&self) -> Result<Vec<String>> {
        Ok(self
            .skip_until(&Regex::new(r"Table of Contents").unwrap())
            .context("Failed to find find TOC header")?
            .into_iter()
            .skip_while(|s| !s.is_empty())
            .collect())
    }

    fn take_section(&self) -> Result<Vec<String>> {
        Ok(self[1..]
            .take_until(&Regex::new(r"^\d.\d? ((\w*) ?)+").unwrap())
            .unwrap_or_default())
    }

    fn skip_to_section(&self, section: &str) -> Result<Vec<String>> {
        self.skip_until(&Regex::new(section).unwrap())
            .with_context(|| format!("Section {} not found", section))
            .into()
    }

    fn table_row(&self, name: &str) -> Result<TableRow> {
        let column_regex = Regex::new(&format!(r"\| {} *\|", name)).unwrap();
        let rows = self.iter().filter(|l| column_regex.is_match(l)).collect::<Vec<_>>();

        match rows.len() {
            0 => Err(anyhow!("Found no rows matching {}", name)),
            1 => TableRow::new(rows[0].clone()),
            _ => Err(anyhow!("Found multiple rows matching {}", name)),
        }
    }

    // Concrete functions for getting values
    fn slice_luts(&self) -> Result<usize> {
        self
            .strip_toc()?
            .skip_to_section("1. Slice Logic")?
            .take_section()?
            .table_row("Slice LUTs")?
            .column(1, |s| s.parse::<usize>().context("Expected slices to be a usize"))
    }

    fn slice_registers(&self) -> Result<usize> {
        self
            .strip_toc()?
            .skip_to_section("1. Slice Logic")?
            .take_section()?
            .table_row("Slice Registers")?
            .column(1, |s| s.parse::<usize>().context("Expected slices to be a usize"))
    }

    fn slices(&self) -> Result<usize> {
        self
            .strip_toc()?
            .skip_to_section("2. Slice Logic Distribution")?
            .take_section()?
            .table_row(r"Slice  ")?
            .column(1, |s| s.parse::<usize>().context("Expected slices to be a usize"))
    }

    fn bram(&self) -> Result<usize> {
        self
            .strip_toc()?
            .skip_to_section("3. Memory")?
            .take_section()?
            .table_row(r"Block RAM Tile")?
            .column(1, |s| s.parse::<usize>().context("Expected slices to be a usize"))
    }
}


pub trait XilinxTimingExt {
    fn period(&self) -> Result<f32>;
}

impl XilinxTimingExt for [String] {
    fn period(&self) -> Result<f32> {
    let re = Regex::new(r#"Min Period\s*n/a\s*DSP48E1/CLK\s+n/a\s+(\d+.\d+)"#).unwrap();
        let period_str = self.group_at_only_match(&re, 1)
            .with_context(|| format!("Could not find min period"))?;

        period_str
            .parse()
            .with_context(|| format!("failed to parse period '{}' as f32", period_str))
    }
}

#[derive(Debug)]
pub struct TableRow {
    columns: Vec<String>,
}

impl std::fmt::Display for TableRow {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.columns.join("|"))
    }
}

impl TableRow {
    pub fn new(inner: String) -> Result<Self> {
        lazy_static! {
            static ref COLUMN_REGEX: Regex = Regex::new(r"\|([^|]+)").unwrap();
        }

        if !COLUMN_REGEX.is_match(&inner) {
            Err(anyhow!("Expected {} to be a column", inner))?
        }

        let columns = COLUMN_REGEX
            .captures_iter(&inner)
            .map(|c| {
                c.get(1)
                    .expect("Expected one capture group")
                    .as_str()
                    .trim()
                    .to_string()
            })
            .collect();

        Ok(Self { columns })
    }

    pub fn column<T>(&self, index: usize, conversion: impl Fn(&str) -> Result<T>) -> Result<T> {
        conversion(
            &self
                .columns
                .get(index)
                .ok_or(anyhow!("Expected to find a column {}", index))?,
        )
    }
}
