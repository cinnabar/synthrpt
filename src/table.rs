use anyhow::{Result, Context};
use std::path::{Path, PathBuf};

pub struct Column<'a> {
    pub header: &'a str,
    pub value: Box<dyn Fn(&Path) -> Result<String> + 'a>,
}

pub struct Table<'a> {
    pub columns: Vec<Column<'a>>,
}

impl<'a> Table<'a> {
    pub fn generate(&self, files: &[PathBuf]) -> Result<String> {
        let mut cells: Vec<Vec<String>> = vec![];

        for column in self.columns.iter() {
            let mut this_column = vec![];
            this_column.push(column.header.to_string());
            for file in files {
                this_column.push((column.value)(file).with_context(|| format!("While reading {}", file.to_string_lossy()))?)
            }

            let max_width = this_column
                .iter()
                .map(|v| v.len())
                .max()
                .expect("Expected at least one row");

            cells.push(
                this_column
                    .into_iter()
                    .map(|v| {
                        format!(
                            "{}{}",
                            v,
                            (0..(max_width - v.len())).map(|_| ' ').collect::<String>(),
                        )
                    })
                    .collect(),
            );
        }

        let mut rows = vec![];
        for row_idx in 0..(cells[0].len()) {
            let mut row = vec![];
            for col_idx in 0..cells.len() {
                row.push(cells[col_idx][row_idx].clone())
            }

            rows.push(row.join(", "));
        }

        Ok(rows.join("\n"))
    }
}
