use anyhow::{Context, Result, anyhow};
use regex::Regex;

use crate::parseutil::LinesExt;

pub trait NextpnrExt {
    // Utility functions
    fn pre_packing_section(&self) -> Result<Vec<String>>;
    fn device_utilisation_section(&self) -> Result<Vec<String>>;
    /// Returns the utilisation value from a row on the form
    /// info: <name>: <used>/<total> <percentage>%
    /// Return an error if more than one row matches
    fn util_row(&self, name: &str) -> Result<usize>;

    // Extraction functions
    fn lut4s(&self) -> Result<usize>;
    fn dffs(&self) -> Result<usize>;
    fn slices(&self) -> Result<usize>;
}

impl NextpnrExt for [String] {
    fn pre_packing_section(&self) -> Result<Vec<String>> {
        self.skip_until(&Regex::new("Logic utilisation before packing").unwrap())?
            .take_until(&Regex::new("Packing IOs").unwrap())
    }

    fn device_utilisation_section(&self) -> Result<Vec<String>> {
        self.skip_until(&Regex::new("Device utilisation:").unwrap())?
            .take_until(&Regex::new(r"^$").unwrap())
    }

    fn util_row(&self, name: &str) -> Result<usize> {
        let column_regex = Regex::new(&format!(r"Info:\s*{}:\s*(\d+)/\d+\s+\d+%", name)).unwrap();
        let capture = self.group_at_only_match(&column_regex, 1)
            .with_context(|| format!("When looking for row {}", name))?;

        capture
            .parse::<usize>()
            .context(format!("{} was not a number", capture))
            .with_context(|| format!("When looking for row {}", name))
    }

    fn lut4s(&self) -> Result<usize> {
        self.pre_packing_section()?.util_row("Total LUT4s")
    }

    fn dffs(&self) -> Result<usize> {
        self.pre_packing_section()?.util_row("Total DFFs")
    }

    fn slices(&self) -> Result<usize> {
        self.device_utilisation_section()?.util_row("TRELLIS_SLICE")
    }
}
