use std::path::{Path, PathBuf};

use anyhow::{Context, Result};
use structopt::{clap::arg_enum, StructOpt};

use crate::{altera_parsing::AlteraExt, metadata::Metadata, nextpnr_parsing::NextpnrExt, table::{Column, Table}, xilinx_parsing::{XilinxExt, XilinxTimingExt}};

mod metadata;
mod nextpnr_parsing;
mod parseutil;
mod table;
mod xilinx_parsing;
mod altera_parsing;

fn lines_in_dir<T>(
    file: PathBuf,
    then: impl Fn(&[String]) -> Result<T>,
) -> impl Fn(&Path) -> Result<String>
where
    T: std::fmt::Display,
{
    let file = file.clone();
    move |dir: &Path| {
        let full_filename = dir.join(file.clone());

        let lines = String::from_utf8_lossy(
            &std::fs::read(&full_filename)
                .with_context(|| format!("Failed to read {:?}", full_filename))?,
        )
        .lines()
        .map(String::from)
        .collect::<Vec<_>>();

        Ok(format!("{}", then(&lines)?))
    }
}

arg_enum! {
    #[derive(StructOpt)]
    enum Tool {
        Xilinx,
        Nextpnr,
        Altera
    }
}

#[derive(StructOpt)]
struct Opt {
    #[structopt()]
    tool: Tool,
    #[structopt()]
    dirs: Vec<PathBuf>,
    #[structopt(long)]
    ignore_flags: Vec<String>,
}

fn xilinx_columns() -> Vec<Column<'static>> {
    vec![
        Column {
            header: "slice_luts",
            value: Box::new(lines_in_dir(
                PathBuf::from("result/post_place_util_report.rpt"),
                XilinxExt::slice_luts,
            )),
        },
        Column {
            header: "slice_registers",
            value: Box::new(lines_in_dir(
                PathBuf::from("result/post_place_util_report.rpt"),
                XilinxExt::slice_registers,
            )),
        },
        Column {
            header: "slices",
            value: Box::new(lines_in_dir(
                PathBuf::from("result/post_place_util_report.rpt"),
                XilinxExt::slices,
            )),
        },
        Column {
            header: "brams",
            value: Box::new(lines_in_dir(
                PathBuf::from("result/post_place_util_report.rpt"),
                XilinxExt::bram,
            )),
        },
        Column {
            header: "min_period",
            value: Box::new(lines_in_dir(
                PathBuf::from("result/post_route_timing_summary.rpt"),
                XilinxTimingExt::period,
            )),
        },
    ]
}

fn nextpnr_columns() -> Vec<Column<'static>> {
    vec![
        Column {
            header: "lut4",
            value: Box::new(lines_in_dir(
                PathBuf::from("synth/pnr.log"),
                NextpnrExt::lut4s,
            )),
        },
        Column {
            header: "DFFs",
            value: Box::new(lines_in_dir(
                PathBuf::from("synth/pnr.log"),
                NextpnrExt::dffs,
            )),
        },
        Column {
            header: "Slices",
            value: Box::new(lines_in_dir(
                PathBuf::from("synth/pnr.log"),
                NextpnrExt::slices,
            )),
        },
    ]
}

fn altera_columns() -> Vec<Column<'static>> {
    vec![
        Column {
            header: "alms",
            value: Box::new(lines_in_dir(
                PathBuf::from("_module_.fit.rpt"),
                AlteraExt::alms,
            )),
        },
        Column {
            header: "luts_and_regs",
            value: Box::new(lines_in_dir(
                PathBuf::from("_module_.fit.rpt"),
                AlteraExt::lut_logic_and_registers,
            )),
        },
        Column {
            header: "luts",
            value: Box::new(lines_in_dir(
                PathBuf::from("_module_.fit.rpt"),
                AlteraExt::lut_logic,
            )),
        },
        Column {
            header: "regs",
            value: Box::new(lines_in_dir(
                PathBuf::from("_module_.fit.rpt"),
                AlteraExt::registers,
            )),
        },
    ]
}


fn base_columns<'a>(
    ignored_flags: Vec<String>,
    mut specific_columns: Vec<Column<'static>>,
) -> Vec<Column<'static>> {
    let mut result = vec![
        Column {
            header: "seed",
            value: Box::new(metadata::seed),
        },
        Column {
            header: "wl",
            value: Box::new(metadata::get_metadata(
                PathBuf::from("metadata.toml"),
                Metadata::actual_word_length,
            )),
        },
    ];

    result.append(&mut specific_columns);

    result.append(&mut vec![
        Column {
            header: "opt_level",
            value: Box::new(metadata::get_metadata(
                PathBuf::from("metadata.toml"),
                |m| Ok(m.opt.clone()),
            )),
        },
        Column {
            header: "opt_flags",
            value: Box::new(metadata::get_metadata(
                PathBuf::from("metadata.toml"),
                move |m| {
                    let all_flags = m.opt_flags.trim_matches(|c| c == '\'').split(" ");

                    Ok(all_flags
                        .filter(|f| !ignored_flags.contains(&f.to_string()))
                        .collect::<Vec<_>>()
                        .join(" "))
                },
            )),
        },
        Column {
            header: "model_name",
            value: Box::new(metadata::get_metadata(
                PathBuf::from("metadata.toml"),
                |m| Ok(m.model.clone()),
            )),
        },
    ]);

    result
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let tool_columns = match opt.tool {
        Tool::Xilinx => xilinx_columns,
        Tool::Nextpnr => nextpnr_columns,
        Tool::Altera => altera_columns,
    };

    let table = Table {
        columns: base_columns(opt.ignore_flags, tool_columns()),
    };

    println!("{}", table.generate(&opt.dirs)?);

    // println!("{}", report_lines.join("\n"));
    // println!("{}", slices);

    Ok(())
}
