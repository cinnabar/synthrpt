use anyhow::{Context, Result};
use regex::Regex;

use crate::parseutil::LinesExt;

pub trait AlteraExt {
    fn skip_toc(&self) -> Result<Vec<String>>;
    fn find_fitter_resource_usage_table(&self) -> Result<Vec<String>>;

    fn fitter_resource_row(&self, description: &str, row_regex: &Regex) -> Result<usize>;

    fn alms(&self) -> Result<usize>;
    fn lut_logic_and_registers(&self) -> Result<usize>;
    fn lut_logic(&self) -> Result<usize>;
    fn registers(&self) -> Result<usize>;
}

impl AlteraExt for [String] {
    fn skip_toc(&self) -> Result<Vec<String>> {
        self.skip_until(&Regex::new(r"; Legal Notice ;").unwrap())
            .context("When trying to skip TOC")
    }

    fn find_fitter_resource_usage_table(&self) -> Result<Vec<String>> {
        let divider_regex = Regex::new(r"\+-+\+").unwrap();
        self.skip_toc()?
            .skip_until(&Regex::new("Fitter Resource Usage Summary").unwrap())
            .context("When trying to find fitter resource usage summary")?
            // Skip the header
            .skip_until(&divider_regex)?
            .skip_until(&divider_regex)?
            .take_until(&divider_regex)
    }

    fn fitter_resource_row(&self, description: &str, row_regex: &Regex) -> Result<usize> {
        self.find_fitter_resource_usage_table()?
            .group_at_only_match(&row_regex, 1)?
            .replace(",", "")
            .parse::<usize>()
            .context(format!("When looking for {}", description))
    }


    fn alms(&self) -> Result<usize> {
        let re = Regex::new(r"ALMs used in final placement \[=a\+b\+c\+d]\s*; (\d{1,3}(,\d{3})*) / \d+").unwrap();

        self.fitter_resource_row("alms", &re)
    }

    fn lut_logic(&self) -> Result<usize> {
        let re =
            Regex::new(r"ALMs used for LUT logic\s*; (\d{1,3}(,\d{3})*)").unwrap();
        self.fitter_resource_row("lut_logic", &re)
    }

    fn lut_logic_and_registers(&self) -> Result<usize> {
        let re =
            Regex::new(r"ALMs used for LUT logic and registers\s*; (\d{1,3}(,\d{3})*)").unwrap();
        self.fitter_resource_row("lut logic and registers", &re)
    }

    fn registers(&self) -> Result<usize> {
        let re =
            Regex::new(r"ALMs used for registers\s*; (\d{1,3}(,\d{3})*)").unwrap();
        self.fitter_resource_row("lut logic and registers", &re)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn synthesis_results_with_thousands_separators_work() {
        let report = String::from_utf8_lossy(include_bytes!("../example_reports/altera/large_design.fit.rpt"))
            .lines().map(String::from).collect::<Vec<_>>();

        assert_eq!(report.alms().expect("expected to find alms"), 14939);
        assert_eq!(report.lut_logic().expect("expected to find lut logic"), 2083);
        assert_eq!(report.lut_logic_and_registers().expect("expected to find lut logic"), 6542);
        assert_eq!(report.registers().expect("expected to find lut logic"), 5114);
    }
}
