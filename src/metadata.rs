use std::path::{Path, PathBuf};

use anyhow::{Context, Result};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Metadata {
    pub bench_version: String,
    pub cinnabar_version: String,
    pub limestone_version: String,
    pub description: String,

    pub word_length: String,
    pub opt: String,
    pub model: String,
    pub opt_flags: String,
}

impl Metadata {
    // Some idiot decided that word length should be a string, and be off by one
    // since we have a sign bit
    pub fn actual_word_length(&self) -> Result<usize> {
        Ok(self
            .word_length
            .parse::<usize>()
            .context("Word length was not an integer")?
            + 1)
    }
}

pub fn get_metadata<T>(
    file: PathBuf,
    getter: impl Fn(&Metadata) -> Result<T>,
) -> impl Fn(&Path) -> Result<String>
where
    T: std::fmt::Display,
{
    let file = file.clone();
    move |dir: &Path| {
        let full_filename = dir.join(file.clone());

        let bytes = std::fs::read(&full_filename)
            .with_context(|| format!("Failed to read {:?}", full_filename))?;

        let content = String::from_utf8_lossy(&bytes);

        let m = toml::from_str(&content)?;

        Ok(format!("{}", getter(&m)?))
    }
}

pub fn seed(dir: &Path) -> Result<String> {
    let full_path = dir.join("seed");
    let option = if full_path.exists() {
        let bytes = std::fs::read(full_path).context("failed to read seed file")?;

        Some(
            String::from_utf8_lossy(&bytes)
                .trim()
                .parse::<usize>()
                .context("seed was not a number")?,
        )
    } else {
        None
    };

    Ok(option.map(|s| format!("{}", s)).unwrap_or_default())
}
