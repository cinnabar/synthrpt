use anyhow::{anyhow, Result};
use regex::Regex;

pub trait LinesExt {
    fn skip_until(&self, re: &Regex) -> Result<Vec<String>>;
    fn take_until(&self, re: &Regex) -> Result<Vec<String>>;
    fn group_at_only_match(&self, re: &Regex, group: usize) -> Result<String>;
}

impl LinesExt for [String] {
    fn skip_until(&self, re: &Regex) -> Result<Vec<String>> {
        let first_index = self
            .iter()
            .enumerate()
            .filter(|(_, l)| re.is_match(l))
            .next()
            .ok_or(anyhow!("No lines matched {}", re))?
            .0;

        Ok(self[first_index + 1..].into())
    }

    fn take_until(&self, re: &Regex) -> Result<Vec<String>> {
        let first_index = self
            .iter()
            .enumerate()
            .filter(|(_, l)| re.is_match(l))
            .next()
            .ok_or(anyhow!("No lines matched {}", re))?
            .0;

        Ok(self[..first_index].into())
    }

    /// Tries to match `re` on every line. If exactly one match is found,
    /// return the group at the specified index. Otherwise return an error
    fn group_at_only_match(&self, re: &Regex, group: usize) -> Result<String> {
        let rows = self
            .iter()
            .filter(|l| re.is_match(l))
            .collect::<Vec<_>>();

        match rows.as_slice() {
            [] => Err(anyhow!("Found no lines matching {:?}", re)),
            [m] => {
                Ok(re
                    .captures(m)
                    .unwrap()
                    .get(group)
                    .ok_or(anyhow!("Expected group {} to be present\nLine: {}\nRegex: {:?}", group, m, re))?
                    .as_str()
                    .to_string()
                )
            }
            _ => Err(anyhow!("Found multiple lines matching {:?}", re)),
        }
    }
}

